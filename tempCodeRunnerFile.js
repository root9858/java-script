function multi_Matrices(m1, m2) {
    let resultado = [];
    for (let i = 0; i < m1.length; i++) {
        resultado[i] = [];
        for (let j = 0; j < m2[0].length; j++) {
            let sum = 0;
            for (let k = 0; k < m1[0].length; k++) {
                sum += m1[i][k] * m2[k][j];
            }
            resultado[i][j] = sum;
        }
    }
    return resultado;
}

let m1 = [[2,-1],[2,0]]
let m2 = [ [2,3],[-2,1] ]

let x = multi_Matrices(m1, m2)



console.log(x) 